#!/bin/sh

if [ -d "protondb-data" ]; then
	cd "protondb-data"
	git pull > /dev/zero
	cd ..
else
	git submodule init > /dev/zero
	git submodule update > /dev/zero
fi

if [ -d "ac-data" ]; then
	cd "ac-data"
	git pull > /dev/zero
	cd ..
else
	git submodule init > /dev/zero
	git submodule update > /dev/zero
fi

if [ -d "vr-data" ]; then
	cd "vr-data"
	git pull > /dev/zero
	cd ..
else
	git submodule init > /dev/zero
	git submodule update > /dev/zero
fi

python calc_trend.py > resources/trend.csv

cd resources

gnuplot -p plot_trend.gnuplot
gnuplot -p plot_trend_stacked.gnuplot
