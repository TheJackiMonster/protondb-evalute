#!/bin/python

import sys

from protondb import Database, Rating, ACList, VRList


if len(sys.argv) > 1:
    database = Database()
    ac_list = ACList()
    vr_list = VRList()

    for i in range(len(sys.argv) - 1):
        database.load(sys.argv[1 + i])
    
    ac_list.load('ac-data/games.json')
    vr_list.load('vr-data/Proton/Proton_Steam.md')

    ratings = database.getRatings([ac_list, vr_list])
    total_ratings = 0

    for value in ratings.values():
        total_ratings += value

    good = 0
    row = ""

    for (rating, value) in ratings.items():
        percentage = int(100 * value / total_ratings)

        if (rating == Rating.GOLD) or (rating == Rating.PLATINUM) or (rating == Rating.NATIVE): # GOLD++
            good = good + value

        print(rating.name + ": " + str(percentage) + "% ( " + str(value) + " / " + str(total_ratings) + " )")

    print()
    print("GOLD++: " + str(int(100 * good / total_ratings)) + "%")
