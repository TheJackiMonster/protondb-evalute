#!/bin/sh

if [ -d "protondb-data" ]; then
	cd "protondb-data"
	git pull > /dev/zero
	cd ..
else
	git submodule init > /dev/zero
	git submodule update > /dev/zero
fi

if [ -d "ac-data" ]; then
	cd "ac-data"
	git pull > /dev/zero
	cd ..
else
	git submodule init > /dev/zero
	git submodule update > /dev/zero
fi

if [ -d "vr-data" ]; then
	cd "vr-data"
	git pull > /dev/zero
	cd ..
else
	git submodule init > /dev/zero
	git submodule update > /dev/zero
fi

old="protondb-data/reports/reports_nov1_2019.tar.gz"
newest=$(ls protondb-data/reports/ | awk -F"[_.]" '/reports/{print substr($2, 4, 1) " " substr($2, 1, 3) " " $3}' | date -f - "+%Y-%m-%d" | sort -r | head -n 1 | LC_ALL=C date -f - +"reports_%b%-d_%Y.tar.gz" | tr A-Z a-z | awk '{print "protondb-data/reports/" $1}')

python "eval_protondb.py" $@ "$old" "$newest"
