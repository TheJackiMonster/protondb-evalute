#!/bin/python

from protondb.rating import Rating


class List:

  def __init__(self):
    self.supported = []
    self.unsupported = []
  
  def clear(self):
    self.supported.clear()
    self.unsupported.clear()
  
  def load(self, path: str) -> bool:
    return False
  
  def isSupported(self, id) -> bool:
    return id in self.supported
  
  def isUnsupported(self, id) -> bool:
    return id in self.unsupported
  
  def getRating(self, id, rating: Rating) -> Rating:
    if self.isSupported(id):
      if rating == Rating.BORKED or rating == Rating.BRONZE:
        return Rating.SILVER
      else:
        return rating
    elif self.isUnsupported(id):
      return Rating.BORKED
    else:
      return rating

