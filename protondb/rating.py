#!/bin/python

from enum import Enum, unique


@unique
class Rating(Enum):
    BORKED = 0
    BRONZE = 1
    SILVER = 2
    GOLD = 3
    PLATINUM = 4
    NATIVE = 5

    @classmethod
    def byName(cls, name: str):
        if name is None:
            return None

        for rating in Rating:
            if rating.name == name.upper():
                return rating

        return None

    @classmethod
    def byReportResponses(cls, responses):
        installs = responses.get('installs', None) == "yes"
        opens = responses.get('opens', None) == "yes"

        if (not installs) or (not opens):
            return Rating.BORKED

        startsPlay = responses.get('startsPlay', None) == "yes"
        stabilityFaults = responses.get('stabilityFaults', None) != "no"
        significantBugs = responses.get('significantBugs', None) != "no"

        if (not startsPlay) or stabilityFaults or significantBugs:
            return Rating.BRONZE

        saveGameFaults = responses.get('saveGameFaults', None) != "no"
        verdict = responses.get('verdict', None) == "yes"

        if saveGameFaults or (not verdict):
            return Rating.SILVER

        protonVersion = responses.get('protonVersion', None)
        performanceFaults = responses.get('performanceFaults', None) != "no"
        graphicalFaults = responses.get('graphicalFaults', None) != "no"
        windowingFaults = responses.get('windowingFaults', None) != "no"
        audioFaults = responses.get('audioFaults', None) != "no"
        inputFaults = responses.get('inputFaults', None) != "no"

        if protonVersion != "Default" or \
                performanceFaults or graphicalFaults or windowingFaults or \
                audioFaults or inputFaults:
            return Rating.GOLD

        return Rating.PLATINUM
