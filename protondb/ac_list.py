#!/bin/python

import json

from protondb.list import List


class ACList(List):

  def __init__(self):
    List.__init__(self)
  
  def load(self, path: str):
    with open(path, "r", encoding="utf-8") as file:
      dataEntries = json.load(file)

      for data in dataEntries:
        store_ids = data.get('storeIds', {})
        steam_id = store_ids.get('steam', None)

        if steam_id is None:
          continue

        status = data.get('status', None)

        if status == 'Supported' or status == 'Running':
          self.supported.append(steam_id)
        elif status == 'Denied' or status == 'Borked':
          self.unsupported.append(steam_id)

    return True
