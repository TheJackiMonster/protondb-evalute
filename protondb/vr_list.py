#!/bin/python

from protondb.list import List


class VRList(List):

  def __init__(self):
    List.__init__(self)
  
  def load(self, path: str):
    lines = []
    prefix = "[*][url=https://store.steampowered.com/app/"

    with open(path, "r", encoding="utf-8") as file:
      lines = filter(
        lambda line: line.startswith(prefix),
        file.readlines()
      )

      for line in lines:
        id_line = line[len(prefix):]

        try:
          index = id_line.index("]")
          steam_id = str(int(id_line[:index], 10))

          self.supported.append(steam_id)
        except ValueError:
          continue

    return True
