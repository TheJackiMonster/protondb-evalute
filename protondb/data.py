#!/bin/python

from datetime import datetime
import multiprocessing
import os

from protondb.database import Database


def loadReportDatabase(_date, _path):
    database = Database()

    oldReportsDate = datetime(2019, 11, 1)
    reportsPath = os.path.split(_path)[0]

    if _date > oldReportsDate:
        if not database.load(os.path.join(reportsPath, "reports_nov1_2019.tar.gz")):
            return None

    if not database.load(_path):
        return None

    return {'date': _date, 'ratings': database.getRatings()}


class Data:

    def __init__(self):
        self.ratings = list()

    def load(self, path: str):
        if not os.path.isdir(path):
            return False

        reports_path = os.path.join(path, "reports")

        if not os.path.isdir(reports_path):
            return False

        reports = list()

        for path in os.listdir(reports_path):
            parts = path.split('.')

            if (len(parts) < 3) or (parts[-1] != "gz") or (parts[-2] != "tar"):
                continue

            parts = '.'.join(parts[:-2]).split('_')

            if (len(parts) < 3) or (parts[0] != "reports"):
                continue

            date = datetime.strptime('_'.join(parts[1:]), '%b%d_%Y')

            reports.append({'date': date, 'path': os.path.join(reports_path, path)})

        reports.sort(key=lambda report: report['date'])

        pool = multiprocessing.Pool(multiprocessing.cpu_count())

        results = [pool.apply_async(loadReportDatabase, args=(report['date'], report['path'])) for report in reports]

        for result in results:
            self.ratings.append(result.get())

        pool.close()
        pool.join()
        return True
