#!/bin/python

import json
import tarfile

from protondb.rating import Rating
from protondb.report import Report
from protondb.list import List


class Database:

    def __init__(self):
        self.reports = dict()

    def clear(self):
        self.reports.clear()

    def load(self, path: str):
        tar = tarfile.open(path, "r:gz")

        if tar is None:
            return False

        for member in tar.getmembers():
            file = tar.extractfile(member)

            if file is None:
                continue

            dataEntries = json.load(file)

            for data in dataEntries:
                report = Report()

                if (not report.loadReport(data)) or (not report.isValid()):
                    continue

                if not report.id in self.reports:
                    self.reports[report.id] = list()

                self.reports[report.id].append(report)

        tar.close()
        return True

    def getRating(self, id) -> Rating:
        stats = list()

        for rating in Rating:
            stats.append({'key': rating, 'value': 0})

        for report in self.reports.get(id, []):
            if report.rating is None:
                continue

            stats[report.rating.value]['value'] += 1

        stats.sort(reverse=True, key=lambda stat: stat['value'])

        if stats[0].get('value', 0) == 0:
            return None

        return stats[0].get('key', None)

    def getRatings(self, lists = []):
        ratings = dict()

        for rating in Rating:
            ratings[rating] = 0

        for id in self.reports.keys():
            rating = self.getRating(id)

            for l in lists:
                rating = l.getRating(id, rating)

            if rating is None:
                continue

            ratings[rating] += 1

        return ratings
