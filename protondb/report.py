#!/bin/python

import requests

from protondb.rating import Rating


class Report:

    def __init__(self):
        self.id = None

        self.title = None
        self.rating = None

        self.specs = None
        self.os = None
        self.driver = None
        self.proton = None

    def isValid(self):
        return self.id is not None

    def loadOldReport(self, data):
        if not 'appId' in data:
            return False

        self.id = data.get('appId')

        self.title = data.get('title', None)
        self.rating = Rating.byName(data.get('rating', None))

        self.specs = data.get('specs', None)
        self.os = data.get('os', None)
        self.driver = data.get('gpuDriver', None)
        self.proton = data.get('protonVersion', None)

        return True

    def loadNewReport(self, data):
        if not 'responses' in data:
            return False

        responses = data.get('responses')

        if not 'answerToWhatGame' in responses:
            return False

        self.id = responses.get('answerToWhatGame')

        app = data.get('app', {})

        self.title = app.get('title', None)
        self.rating = Rating.byReportResponses(responses)

        systemInfo = data.get('systemInfo', {})

        cpu = systemInfo.get('cpu', None)
        gpu = systemInfo.get('gpu', None)

        self.specs = str(cpu) + " / " + str(gpu)
        self.os = systemInfo.get('os', None)
        self.driver = systemInfo.get('gpuDriver', None)
        self.proton = responses.get('protonVersion', None)

        return True

    def loadReport(self, data):
        if not self.loadNewReport(data):
            return self.loadOldReport(data)
        else:
            return True

    def requestRating(self, trending=False):
        if not self.isValid():
            return None

        response = requests.get(
            "https://www.protondb.com/api/v1/reports/summaries/{}.json".format(self.id)
        )

        if response.status_code != 200:
            return None

        try:
            return Rating.byName(response.json().get('trendingTier' if trending else 'tier', None))
        except ValueError:
            return None

    def requestNativeCompatibility(self):
        if not self.isValid():
            return False

        response = requests.get(
            "https://store.steampowered.com/broadcast/ajaxgetappinfoforcap?appid=".format(self.id)
        )

        if response.status_code != 200:
            return False

        try:
            return response.json().get('available_linux', False)
        except ValueError:
            return False
