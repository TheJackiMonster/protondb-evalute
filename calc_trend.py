#!/bin/python

from protondb import Data, Rating

data = Data()
data.load("protondb-data")

print("DATE " + ' '.join(rating.name for rating in Rating) + " GOLD++ TOTAL")

for entry in data.ratings:
    date = entry['date']
    ratings = entry['ratings']

    total_ratings = 0

    for value in ratings.values():
        total_ratings += value

    good = 0
    row = ""

    for (rating, value) in ratings.items():
        percentage = int(100 * value / total_ratings)

        if (rating == Rating.GOLD) or (rating == Rating.PLATINUM) or (rating == Rating.NATIVE):  # GOLD++
            good = good + value

        if len(row) > 0:
            row = row + " "

        row = row + str(value)

    print(date.strftime('%Y-%m-%d') + " " + row + " " + str(good) + " " + str(total_ratings))
