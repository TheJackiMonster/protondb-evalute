#!/bin/gnuplot -p

reset
clear

set datafile separator " "
set key outside

set xdata time
set timefmt "%Y-%m-%d"

set xlabel "Time (date)"
set xtics out nomirror rotate right

set ylabel "Coverage (rating)"
set ytics out nomirror

firstcol=2
cumsum(i)=((i>firstcol)?column(i)+cumsum(i-1):(i==firstcol)?column(i):1/0)

totalcol=9
relcumsum(i)=cumsum(i)/column(totalcol)

plot "trend.csv" using 1:(relcumsum(6)) title columnheader(6) with filledcurves x1 linestyle 5, "" using 1:(relcumsum(5)) title columnheader(5) with filledcurves x1 linestyle 4, "" using 1:(relcumsum(4)) title columnheader(4) with filledcurves x1 linestyle 3, "" using 1:(relcumsum(3)) title columnheader(3) with filledcurves x1 linestyle 2, "" using 1:(relcumsum(2)) title columnheader(2) with filledcurves x1 linestyle 1
