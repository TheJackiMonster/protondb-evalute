#!/bin/gnuplot -p

reset
clear

set datafile separator " "
set key outside

set xdata time
set timefmt "%Y-%m-%d"

set xlabel "Time (date)"
set xtics out nomirror rotate right

set ylabel "Coverage (rating)"
set ytics out nomirror

totalcol=9
rel(i)=column(i)/column(totalcol)

plot "trend.csv" using 1:(rel(6)) title columnheader(6) with lines linestyle 5, "" using 1:(rel(5)) title columnheader(5) with lines linestyle 4, "" using 1:(rel(4)) title columnheader(4) with lines linestyle 3, "" using 1:(rel(3)) title columnheader(3) with lines linestyle 2, "" using 1:(rel(2)) title columnheader(2) with lines linestyle 1
