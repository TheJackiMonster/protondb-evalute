# ProtonDB Evalute

Evaluates the reports from ProtonDB to give stats about the overall progress.

## How to read

With the written scripts you can retrieve the data and convert it to easily readable charts with GNUplot:

![chart0.png](./resources/chart_0.png)
![chart1.png](./resources/chart_1.png)

The charts shows the relative progress of non-native games in the ProtonDB rating system.

## How to use

### Automatic

You can simply run the shell script to download all reports and 
get the results from all of the newest data, just like this:
```
sh "all_reports.sh"
```

The output should look like this (results from January 2025):
```
BORKED: 12% ( 3726 / 29226 )
BRONZE: 8% ( 2584 / 29226 )
SILVER: 3% ( 1154 / 29226 )
GOLD: 50% ( 14786 / 29226 )
PLATINUM: 23% ( 6976 / 29226 )
NATIVE: 0% ( 0 / 29226 )

GOLD++: 74%
```

### Manual

Clone the repository or download the **"reports"** directory from 
[protondb-data](https://github.com/bdefore/protondb-data).
```
git clone "https://github.com/bdefore/protondb-data.git"
```

Depending on the path of the **"reports"** directory, you can use 
the python script to evaluate all of the reports inside of a specific 
archieve (for example like this):
```
python "eval_protondb.py" "protondb-data/reports/reports_oct3_2020.tar.gz"
```
